public class StrategyInsertionSort implements SortStrategy {
    @Override
    public void sort(int[] arrReference){
        int n = arrReference.length;
        for (int i = 1; i < n; ++i) {
            int key = arrReference[i];
            int j = i - 1;
            while (j >= 0 && arrReference[j] > key) {
                arrReference[j + 1] = arrReference[j];
                j = j - 1;
            }
            arrReference[j + 1] = key;
        }
    }
}
