import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

public class StrategyStandardLibrarySort implements SortStrategy{
    @Override
    public void sort(int[] arrReference) {
        Arrays.sort(arrReference);
    }
}
