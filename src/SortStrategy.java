public interface SortStrategy {
    public void sort(int[] arrReference);
}
