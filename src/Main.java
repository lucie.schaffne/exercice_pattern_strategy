import java.util.Arrays;

public class Main {
    public static void main(String[] args){
        int[] arr = new int[10000];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = (int)(Math.random() *10);
        }

        int[] deepArrCopy1 = arr.clone();
        int[] deepArrCopy2 = arr.clone();
        int[] deepArrCopy3 = arr.clone();

        //On commence par tester selection sort
        Context mySorter = new Context(deepArrCopy1, /*??*/);
        //maintenant on aimerait tester l'algorithme insertion sort sur la 2eme copie
        mySorter.setArray(deepArrCopy2);
        //comment changer la stratégie?
        //mysorter.set_____________?$
        //effectuer la stratégie
        //Finalement, on décide de tester l'algo de la librairie standard java
        mySorter.setArray(deepArrCopy3);
        //changer la stratégie ici
        //mySorter.______?
        //effectuer la stratégie ici
        assert(Arrays.equals(deepArrCopy1,deepArrCopy2));
        assert(Arrays.equals(deepArrCopy2,deepArrCopy3));

    }
}
