public class StrategySelectionSort implements SortStrategy{
    @Override
    public void sort(int[] arrReference) {
        for (int i = 0; i < arrReference.length-1; i++) {
            int indexSmallest=i;
            for (int j = i+1; j <arrReference.length ; j++) {
                //get the index of the smallest elt
                if(arrReference[indexSmallest] > arrReference[j])indexSmallest = j;
            }
            if(i!=indexSmallest){
                arrReference[i] += arrReference[indexSmallest];
                arrReference[indexSmallest] = arrReference[i] - arrReference[indexSmallest];
                arrReference[i]-= arrReference[indexSmallest];
            }
        }
    }
}
